import React from "react";
import { AppStack } from "./navigation";
import { Provider } from "react-redux";
import { store } from "./store/configStore";
import { useFonts } from "@use-expo/font";
import { AppLoading } from "expo";

const customFonts = {
  bold: require("./assets/fonts/JosefinSans-Bold.ttf"),
  regular: require("./assets/fonts/JosefinSans-Regular.ttf"),
  semiBold: require("./assets/fonts/JosefinSans-SemiBold.ttf"),
  ubuntu: require("./assets/fonts/Ubuntu-Regular.ttf")
};
const App = () => {
  const [isLoaded] = useFonts(customFonts);
  if (!isLoaded) {
    return <AppLoading />;
  } else {
    return (
      <Provider store={store}>
        <AppStack />
      </Provider>
    );
  }
};
export default App;
