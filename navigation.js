import React, { useEffect, useState } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { Image } from "react-native";
import {
  createStackNavigator,
  TransitionPresets,
  CardStyleInterpolators
} from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { AppearanceProvider, useColorScheme } from "react-native-appearance";
import { DarkTheme, LightTheme } from "./utility/theme";
import { getUserInfo } from "./store/actions";
import { useDispatch } from "react-redux";

import HomeScreen from "./screens/HomeScreen";
import IntroScreen from "./screens/IntroScreen";
import LinksScreen from "./screens/LinksScreen";
import WelcomeScreen from "./screens/WelcomeScreen";
import LoginScreen from "./screens/LoginScreen";
import RegisterScreen from "./screens/RegisterScreen";
import ProfileScreen from "./screens/ProfileScreen";
import RegisterChoiceScreen from "./screens/RegisterChoiceScreen";
import RegisterProfessionalScreen from "./screens/RegisterProfessionalScreen";
import DomainsScreen from "./screens/DomainsScreen";
import CompaniesScreen from "./screens/CompaniesScreen";
import ProfessionalsScreen from "./screens/ProfessionalsScreen";
import UpdateProfileScreen from "./screens/UpdateProfileScreen";
import ChatScreen from "./screens/ChatScreen";
import RequestScreen from "./screens/RequestScreen";
import SettingScreen from './screens/SettingsScreen';
import firebase from "./utility/Firebase";

const Tab = createBottomTabNavigator();

function MainTabs() {
  return (
    <Tab.Navigator
      tabBarOptions={{
        showIcon: true,
        activeTintColor: "#3B5998",
        inactiveTintColor: "gray"
      }}
    >
      <Tab.Screen
        options={{
          tabBarLabel: "home",
          tabBarIcon: ({ focused }) => (
            <Image
              style={{ width: 30, resizeMode: "contain" }}
              source={
                focused
                  ? require("./assets/images/Tab/homeselected.png")
                  : require("./assets/images/Tab/homeunselected.png")
              }
            />
          )
        }}
        name="Home"
        component={HomeScreen}
      />
      <Tab.Screen
        options={{
          tabBarLabel: "Domains",
          tabBarIcon: ({ focused }) => (
            <Image
              style={{ width: 30, resizeMode: "contain" }}
              source={
                focused
                  ? require("./assets/images/Tab/DomainSelected.png")
                  : require("./assets/images/Tab/DomainUnSelected.png")
              }
            />
          )
        }}
        name="Domain"
        component={DomainsScreen}
      />
      <Tab.Screen
        options={{
          tabBarLabel: "Chat",
          tabBarIcon: ({ focused }) => (
            <Image
              style={{ width: 30, resizeMode: "contain" }}
              source={
                focused
                  ? require("./assets/images/Tab/chatselected.png")
                  : require("./assets/images/Tab/chatunselected.png")
              }
            />
          )
        }}
        name="Links"
        component={LinksScreen}
      />
    </Tab.Navigator>
  );
}

const Stack = createStackNavigator();

export function AppStack() {
  const [user, setUser] = useState();
  const dispatch = useDispatch();
  async function onAuthStateChanged(user) {
    let currentUser = firebase.auth().currentUser;
    if (currentUser) {
      dispatch(getUserInfo(firebase.auth().currentUser.uid));
    }
    setUser(currentUser);
  }
  useEffect(() => {
    const fb = firebase.auth().onAuthStateChanged(onAuthStateChanged);
    return fb;
  }, []);
  const scheme = useColorScheme();
  return (
    <AppearanceProvider>
      <NavigationContainer theme={scheme === "dark" ? DarkTheme : LightTheme}>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
              cardStyleInterpolator:CardStyleInterpolators.forHorizontalIOS,
              
          }}
        >
          {!user ? (
            <>
              <Stack.Screen name="Welcome" component={WelcomeScreen} />
              <Stack.Screen name="Intro" component={IntroScreen} />
              <Stack.Screen name="Register" component={RegisterScreen} />
              <Stack.Screen
                name="RegisterChoice"
                component={RegisterChoiceScreen}
              />
              <Stack.Screen
                name="RegisterAdmin"
                component={RegisterProfessionalScreen}
              />
              <Stack.Screen name="Login" component={LoginScreen} />
            </>
          ) : (
            <>
              <Stack.Screen name="Tabs" component={MainTabs} />
              <Stack.Screen name="Companies" component={CompaniesScreen} />
              <Stack.Screen name="Profile" component={ProfileScreen} />
              <Stack.Screen
                name="Professionals"
                component={ProfessionalsScreen}
              />
              <Stack.Screen name="Chat" component={ChatScreen} />
              <Stack.Screen
                name="UpdateProfile"
                component={UpdateProfileScreen}
              />
              <Stack.Screen name="Settings" component={SettingScreen} />
              <Stack.Screen name="Request" component={RequestScreen} />
            </>
          )}
        </Stack.Navigator>
      </NavigationContainer>
    </AppearanceProvider>
  );
}
