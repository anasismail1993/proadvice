import React, { useState, useEffect, useRef } from "react";
import { Block, Button, Input, NavBar } from "galio-framework";

import {
  Image,
  SafeAreaView,
  TouchableHighlight,
  View,
  Text,
  FlatList,
  ScrollView,
  StyleSheet,
  Dimensions,
  ActivityIndicator,
} from "react-native";
import theme from "../theme";
import { addNewMessage, getMessageConv, setSocketOff } from "../store/actions";
import { useDispatch, useSelector } from "react-redux";
const { width, height } = Dimensions.get("window");
import Firebase from "../utility/Firebase";

import { MonoText } from "../components/StyledText";

const data = [1, 2, 3];
export default function ChatScreen({ navigation, route }) {
  const dispatch = useDispatch();
  const [message, setMessage] = useState("");
  const listMsg = useSelector((state) => state.chat.listMsg);
  const isLoading = useSelector((state) => state.ui.isLoading);

  const { idConv, name } = route.params;
  const myRef = useRef(null);
  useEffect(() => {
    dispatch(getMessageConv(idConv));
    return () => {
      dispatch(setSocketOff(idConv));
    };
  }, []);

  const SendMessageHandler = () => {
    if (message !== "") {
      dispatch(addNewMessage(message, idConv, Firebase.auth().currentUser.uid));
      setMessage("");
    }
  };

  return (
    <View style={{ flex: 1 }}>
     
      <NavBar
        title={name}
        left
        back
        
        onLeftPress={() => {
          navigation.navigate("Links");
        }}
   
        titleStyle={{ fontSize: 24, fontWeight: "700" }}
      />

      <View style={{ flex: 1, justifyContent: "flex-end" }}>
        <ScrollView
          contentContainerStyle={{ flexGrow: 1, justifyContent: "flex-end" }}
          ref={myRef}
          onContentSizeChange={(contentWidth, contentHeight) => {
            myRef.current.scrollToEnd({ animated: true });
          }}
        >
          {isLoading ? (
            listMsg.length > 0 ? (
              listMsg.map((item, index) =>
                item.idUser !== Firebase.auth().currentUser.uid ? (
                  <View
                    key={item.idMsg}
                    style={{
                      width: "90%",
                      height: 50,
                      marginLeft: "5%",
                      marginTop: 5,
                      justifyContent: "flex-start",
                      flexDirection: "row",
                    }}
                  >
                    <Text
                      style={{
                        height: 45,
                        backgroundColor: "#D8D8D8",
                        padding: 10,
                        borderTopRightRadius: 15,
                        borderBottomRightRadius: 15,
                        borderTopLeftRadius: 15,
                      }}
                    >
                      {item.msg}
                    </Text>
                  </View>
                ) : (
                  <View
                    key={item.idMsg}
                    style={{
                      width: "90%",
                      height: 50,
                      marginLeft: "5%",
                      marginTop: 5,
                      justifyContent: "flex-end",
                      flexDirection: "row",
                    }}
                  >
                    <Text
                      style={{
                        height: 45,
                        color: "white",
                        backgroundColor: "#3B5998",
                        padding: 10,
                        borderTopRightRadius: 15,
                        borderBottomLeftRadius: 15,
                        borderTopLeftRadius: 15,
                      }}
                    >
                      {item.msg}
                    </Text>
                  </View>
                )
              )
            ) : (
              <View
                style={{
                  flex: 1,
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <View>
                  <Image
                    style={{ width: 240, height: 220, alignSelf: "center" }}
                    source={require("../assets/emptychat.png")}
                  />
                </View>
              </View>
            )
          ) : (
            <View style={{ flex: 1 }}>
              <ActivityIndicator size="large" color="#0000ff" />
            </View>
          )}
        </ScrollView>
      </View>
      <View
        style={{
          flexDirection: "row",
          height: 64,
          alignItems: "center",
          justifyContent: "space-between",
          paddingLeft: 10,
          paddingRight: 10,
        }}
      >
        <Input
          rounded
          type="email-address"
          color={theme.COLORS.INFO}
          placeholder="Type in your message ..."
          autoCapitalize="none"
          style={{
            width: width * 0.85,
            borderColor: theme.COLORS.INFO,
            elevation: 5,
          }}
          onChangeText={(message) => setMessage(message)}
          value={message}
        />
        <Button
          onlyIcon
          icon="arrowup"
          iconFamily="antdesign"
          iconSize={30}
          color="info"
          iconColor="#fff"
          style={{ width: 40, height: 40, marginLeft: 5 }}
          onPress={() => SendMessageHandler()}
        >
          warning
        </Button>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-around",
    paddingTop: theme.SIZES.BASE * 0.3,
    paddingHorizontal: theme.SIZES.BASE,
    backgroundColor: theme.COLORS.WHITE,
  },
});
