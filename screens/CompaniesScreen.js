import React, { useState ,useEffect,Component } from "react";
import {
    View,TouchableHighlight,StyleSheet,SafeAreaView,ScrollView,TouchableOpacity,FlatList,Image,Dimensions} from "react-native";
import {Block, Card, Text, Icon, NavBar} from 'galio-framework';
const { width,height } = Dimensions.get('window')
import theme from "../theme";
import 'firebase/firestore';
import firebase from '../utility/Firebase'
import { getCompanies } from '../store/actions'
import { useDispatch,useSelector } from 'react-redux'
const bgImage = 'https://images.unsplash.com/photo-1516651029879-bcd191e7d33b?fit=crop&w=900&q=80';

export default function CompaniesScreen({ navigation,route }){
  const dispatch = useDispatch();
  const { domaineId } = route.params;
  const { itemId } = route.params;

  const CompaniesListe = useSelector((state)=> state.companies.CompaniesListe);
  const isLoading = useSelector((state)=> state.ui.isLoading);
  useEffect(()=>{
    dispatch(getCompanies(domaineId))
       },[])
  return (
    
 <View style={{flex:1,paddingTop:20}}>
<Text style={{ fontSize: 24, fontWeight: "700", paddingHorizontal: 20 }}>
              Choose Your Company
              
            </Text>
            {!isLoading && 
   <FlatList
        data={CompaniesListe}
        extraData={CompaniesListe}
        ListEmptyComponent={()=>   <View> 
          <Image style={{width:200,height:200 ,alignSelf:'center'}}
  source={require('../assets/empty.jpg')}/>
  <Text style={{ fontSize: 24, fontWeight: "700", paddingHorizontal: 20 , alignSelf:'center'}}> Empty! for now </Text>
       </View>}
        renderItem={({ item }) =>        
        <TouchableOpacity activeOpacity={0.9} onPress={() => navigation.navigate('Professionals',{companyId:item.id})}
        style={{width:'95%',elevation:3,overflow:'hidden',height:350,backgroundColor:'white', marginBottom:20,marginTop:1,alignSelf:'center',borderTopLeftRadius:20,borderTopRightRadius:20,borderBottomLeftRadius:20,borderBottomRightRadius:20}}>
<Image style={{width:'100%',height:'55%',resizeMode:'stretch'}} source={{uri:item.img.src}} />
<Text style={{ fontSize: 24, paddingHorizontal: 20 ,fontFamily:'bold'}}>
              {item.title}
            </Text>
            
            
            <Text style={{ fontSize: 12,paddingHorizontal:20 ,color:'#5B64F6',fontFamily:'regular'}}>
              {item.adress}
            </Text>
            <Text ellipsizeMode={'tail'} numberOfLines={4} style={{ fontSize: 13, fontWeight: "700", paddingHorizontal: 20 , color:'#939597',marginTop:5}}>
           {item.des}
       
            </Text>
          </TouchableOpacity>
        }/>}
 </View>

  );
}




