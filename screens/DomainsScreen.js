import React, { useState ,useEffect, Component } from "react";
import {
    View,TouchableHighlight,FlatList,StyleSheet,SafeAreaView,ScrollView,Image,Dimensions} from "react-native";
import {NavBar,Text, Block, Card} from 'galio-framework';
const { width } = Dimensions.get('window')
import theme from "../theme";
import 'firebase/firestore';
import firebase from '../utility/Firebase'
import { TouchableOpacity } from "react-native-gesture-handler";
import { useDispatch,useSelector } from 'react-redux'
import { getDomains } from '../store/actions'

  

export default function DomainsScreen({ navigation }){
 
      const dispatch = useDispatch();
      const DomainListe = useSelector((state)=> state.domain.domainListe);
      const isLoading = useSelector((state)=> state.ui.isLoading);
      useEffect(()=>{
   dispatch(getDomains())

      },[])
  return (
    

    <SafeAreaView style={{ flex: 1, backgroundColor: "#F6F8F9" }}>
      
      <View style={{ flex: 1,marginTop:10 }}>
       
            <Text style={{ fontSize: 24, fontWeight: "700", paddingHorizontal: 20 }}>
              Choose Your Field
            </Text>


            {!isLoading && 
            <FlatList
        data={DomainListe}
        renderItem={({ item }) =>        
        <TouchableHighlight underlayColor='white' onPress={() => navigation.navigate('Companies',{domaineId:item.id})}>
        <View style={styles.categoriesItemContainer}>
          <Image style={styles.categoriesPhoto}  source={{uri:item.image.src}} />
            <Text style={styles.categoriesName}>{item.title}</Text>
        </View>
      </TouchableHighlight>}
        keyExtractor={item => item.id}
      />}
     

         
        
      </View>
    </SafeAreaView>
  );
}
   
  const styles = StyleSheet.create({
    header: {
      backgroundColor: theme.COLORS.WHITE,
      borderTopLeftRadius: theme.SIZES.BASE * 2,
      borderTopRightRadius: theme.SIZES.BASE * 2,
      paddingVertical: theme.SIZES.BASE * 2,
      paddingHorizontal: theme.SIZES.BASE * 1.5,
      width,
    },
    categoriesItemContainer: {
      flex: 1,
      margin: 10,
      justifyContent: 'center',
      alignItems: 'center',
      height: 215,
      borderColor: '#cccccc',
      borderWidth: 0.5,
      borderRadius: 20,
    },
    categoriesPhoto: {
      width: '100%',
      height: 155,
      borderRadius: 20,
      borderBottomLeftRadius: 0,
      borderBottomRightRadius: 0,
      shadowColor: 'blue',
      shadowOffset: {
        width: 0,
        height: 3
      },
      shadowRadius: 5,
      shadowOpacity: 1.0,
      
    },
    categoriesName: {
      flex: 1,
      fontSize: 20,
      fontWeight: 'bold',
      textAlign: 'center',
      color: '#333333',
      marginTop: 8
    },
    categoriesInfo: {
      marginTop: 3,
      marginBottom: 5
    },
    stats: {
      borderWidth: 0,
      width: width - theme.SIZES.BASE * 2,
      height: theme.SIZES.BASE * 4,
      marginVertical: theme.SIZES.BASE * 0.875,
    },
    title: {
      justifyContent: 'center',
      paddingLeft: theme.SIZES.BASE / 2,
    },
    avatar: {
      width: theme.SIZES.BASE * 2.5,
      height: theme.SIZES.BASE * 2.5,
      borderRadius: theme.SIZES.BASE * 1.25,
    },
    middle: {
      justifyContent: 'center',
    },
    text: {
      fontSize: theme.SIZES.FONT * 0.875,
      lineHeight: theme.SIZES.FONT * 1.25,
    },
  });