import React, { useState, useEffect } from "react";
import {
  View,
  TouchableHighlight,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  Image,
  Dimensions,
  FlatList,
} from "react-native";
import {
  NavBar,
  Text,
  Block,
  Card,
  Button,
  Input,
  Icon,
} from "galio-framework";
const { width } = Dimensions.get("window");
import theme from "../theme";
import "firebase/firestore";
import firebase from "../utility/Firebase";
import { TouchableOpacity } from "react-native-gesture-handler";
import { getFeatured, getDomainsHome } from "../store/actions";
import { useDispatch, useSelector } from "react-redux";

export default function HomeScreen({ navigation }) {
  const dispatch = useDispatch();
  var today = new Date();
  const [name, setName] = useState("");
  const user = useSelector((state) => state.auth.user);
  const isLoading = useSelector((state) => state.ui.isLoadingUser);
  const isLoadingCompany = useSelector((state) => state.ui.isLoadingCompany);
  const isLoadingCompanyHome = useSelector(
    (state) => state.ui.isLoadingCompanyHome
  );

  const featured = useSelector((state) => state.Featured.FeaturedListe);
  const DomainsListHome = useSelector(
    (state) => state.DomainHome.domainListeHome
  );

  useEffect(() => {
    dispatch(getFeatured());
    dispatch(getDomainsHome());
  }, []);

  const uri =
    "https://s3.amazonaws.com/exp-icon-assets/ExpoEmptyManifest_192.png";
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#F6F8F9" }}>
      <View>
        <ScrollView scrollEventThrottle={16}>
          <View style={{ flex: 1, backgroundColor: "#F6F8F9", paddingTop: 20 }}>
            {!isLoading && (
              <View
                style={{
                  justifyContent: "space-between",
                  flexDirection: "row",
                }}
              >
                <View>
                  <Text
                    style={{
                      fontSize: 24,
                      fontWeight: "700",
                      paddingHorizontal: 20,
                    }}
                  >
                    {user.fullName}
                  </Text>
                  <Text
                    style={{
                      fontSize: 12,
                      fontWeight: "700",
                      paddingHorizontal: 20,
                      color: "#ABABB5",
                    }}
                  >
                    {user.role}
                  </Text>
                </View>
                <View style={{ alignItems: "center" }}>
                  {user.role == "pro" && (
                    <View style={{alignItems:'center'}}>
                    <Button
                      onlyIcon
                      icon="adduser"
                      iconFamily="antdesign"
                      iconSize={20}
                      color="transparent"
                      onPress={() => navigation.navigate("Request")}
                    >
                      requests
                    </Button>

                      <Text
                      style={{
                        fontSize: 12,
                        fontWeight: "700",
                        paddingHorizontal: 20,
                        color: "#ABABB5",
                      }}
                    >
                      My Requests
                    </Text>
                    </View>
                    
                  )}
                
                </View>

                <View>
                  <TouchableOpacity
                    onPress={() => navigation.navigate("Profile")}
                  >
                    <Image
                      source={{ uri: user.avatar }}
                      style={{
                        height: 60,
                        width: 60,
                        borderRadius: 500 / 4,
                        marginRight: 20,
                      }}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            )}

            <View
              style={{
                width: "85%",
                alignSelf: "center",
                height: 1,
                backgroundColor: "#d8d8d8",
                marginTop: 8,
              }}
            ></View>
            <Input
              placeholder="Search here ..."
              rounded
              bgColor="#F6F8F9"
              borderless="true"
              icon="find"
              family="antdesign"
              iconSize={14}
              iconColor="black"
            />
            <Text
              style={{ fontSize: 24, fontWeight: "700", paddingHorizontal: 20 }}
            >
              Trending Today
            </Text>

            <View style={{ height: 130, marginTop: 20 }}>
              <FlatList
                data={DomainsListHome}
                horizontal
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                renderItem={({ item }) => (
                  <TouchableOpacity
                    style={{
                      height: 130,
                      width: 130,
                      marginLeft: 20,
                      borderWidth: 0.5,
                      borderColor: "#dddddd",
                    }}
                    onPress={() =>
                      navigation.navigate("Companies", { domaineId: item.id })
                    }
                  >
                    <View style={{ flex: 2 }}>
                      <Image
                        source={{ uri: item.image.src }}
                        style={{
                          flex: 1,
                          width: null,
                          height: null,
                          resizeMode: "cover",
                        }}
                      />
                    </View>
                    <View style={{ flex: 1, paddingLeft: 10, paddingTop: 10 }}>
                      <Text>{item.title}</Text>
                    </View>
                  </TouchableOpacity>
                )}
                keyExtractor={(item) => item.id}
              />
            </View>

            <View style={{ marginTop: 40, paddingHorizontal: 20 }}>
              <Text style={{ fontSize: 24, fontWeight: "700" }}>
                Featured Company
              </Text>
            </View>

         
                <View style={{ flex: 1, alignContent: "center" }}>
                {!isLoadingCompany && (
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate("Professionals", {
                    companyId: featured.id,
                  })
                }
              >
                  <Card
                    flex
                    borderless
                    style={styles.card}
                    title={featured.title}
                    caption={featured.des}
                    avatar="https://emploi-tunisie-travail.com/wp-content/uploads/2019/09/satri.png"
                    location={featured.adress}
                    imageStyle={styles.cardImageRadius}
                    imageBlockStyle={{ padding: theme.SIZES.BASE / 2 }}
                    image="https://scontent.ftun3-1.fna.fbcdn.net/v/t1.0-9/16864942_1355819874507563_1220060576746504548_n.png?_nc_cat=103&_nc_sid=dd9801&_nc_ohc=W4kmjlbc5ZQAX_mFBkg&_nc_ht=scontent.ftun3-1.fna&oh=50eb1f3731c05f8955d42797b2769480&oe=5F17FB1D"
                  />
                  </TouchableOpacity>
                )}
                </View>
              
            

            {/* <TouchableOpacity
                    onPress={() =>
                      navigation.navigate("Professionals", {
                        companyId: featured.id,
                      })
                    }
                  >
                    <Image
                      style={{
                        height: 200,
                        width: width,
                      }}
                      source={{
                        uri: featured.img != undefined && featured.img.src,
                      }}
                    />
                    </TouchableOpacity>
                    
                             <Text
                  style={{
                    fontSize: 24,
                    fontWeight: "700",
                    alignSelf: "center",
                  }}
                >
                  {featured.title}
                </Text>
            
                    */}
          </View>

          <View style={{ marginTop: 40, paddingHorizontal: 20 }}>
            <Text style={{ fontSize: 24, fontWeight: "700" }}>
              Startups
            </Text>
          </View>
          <View style={{ flex: 1, alignContent: "center" }}>
                
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate("Professionals", {
                    companyId: featured.id,
                  })
                }
              >
                  <Card
                    flex
                    borderless
                    style={styles.card}
                    title="JAD PROD"
                    caption="Montages And Shootings"
                    avatar="https://scontent.ftun6-1.fna.fbcdn.net/v/t1.0-9/71471729_118160062920825_4321938608287121408_n.png?_nc_cat=107&_nc_sid=09cbfe&_nc_ohc=n8u1rhBLehoAX9f3d_5&_nc_ht=scontent.ftun6-1.fna&oh=cc67c4e73533ec805a1c0a5473e47921&oe=5F23681F"
                    location="Khzema, sousse"
                    imageStyle={styles.cardImageRadius}
                    imageBlockStyle={{ padding: theme.SIZES.BASE / 2 }}
                    image="https://scontent.ftun6-1.fna.fbcdn.net/v/t1.0-9/71471729_118160062920825_4321938608287121408_n.png?_nc_cat=107&_nc_sid=09cbfe&_nc_ohc=n8u1rhBLehoAX9f3d_5&_nc_ht=scontent.ftun6-1.fna&oh=cc67c4e73533ec805a1c0a5473e47921&oe=5F23681F"
                  />
                  </TouchableOpacity>
                
                </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  header: {
    width,
  },
  categoriesItemContainer: {
    flex: 1,
    margin: 10,
    justifyContent: "center",
    alignItems: "center",
    height: 215,
    borderColor: "#cccccc",
    borderWidth: 0.5,
    borderRadius: 20,
  },
  categoriesPhoto: {
    width: "100%",
    height: 155,
    borderRadius: 20,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    shadowColor: "blue",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowRadius: 5,
    shadowOpacity: 1.0,
  },
  categoriesName: {
    flex: 1,
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center",
    color: "#333333",
    marginTop: 8,
  },
  categoriesInfo: {
    marginTop: 3,
    marginBottom: 5,
  },
  stats: {
    borderWidth: 0,
    width: width - theme.SIZES.BASE * 2,
    height: theme.SIZES.BASE * 4,
  },
  title: {
    justifyContent: "center",
  },

  middle: {
    justifyContent: "center",
  },
  text: {
    fontSize: theme.SIZES.FONT * 0.875,
    lineHeight: theme.SIZES.FONT * 1.25,
  },
});
