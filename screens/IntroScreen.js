import * as WebBrowser from 'expo-web-browser';
import * as React from 'react';
import { Image, Platform, StyleSheet,Dimensions, TouchableOpacity, View , StatusBar} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import {useTheme} from '@react-navigation/native';
import theme from '../theme';
import { Block, Button, NavBar, Text} from 'galio-framework';
import { LinearGradient } from 'expo-linear-gradient';
import Constants from 'expo-constants';


export default function IntroScreen({ navigation }) {
  const {colors} = useTheme();
  const iphoneImage = require('../assets/iphonee.png');
  const { width } = Dimensions.get('screen');
  return (
    <Block flex>
    <StatusBar hidden={true} barStyle="light-content" />
    <Block style={styles.navbar}>
      <NavBar
        transparent
        leftIconColor={theme.COLORS.WHITE}
      />
    </Block>
    <LinearGradient
      colors={['#2F297B', '#ffffff']}
      end={[0.5, 0.9]}
      style={styles.backgroundGradient}
    />
    <Block flex center style={styles.container}>
      <Block flex middle style={{ justifyContent: 'flex-end', marginBottom: theme.SIZES.BASE * 2.5 }}>
        <Text center size={theme.SIZES.FONT * 2.375} color={theme.COLORS.WHITE} style={{ marginBottom: theme.SIZES.BASE }}>
          ProAdvice
        </Text>
        <Text center size={theme.SIZES.FONT * 0.875} color={theme.COLORS.WHITE} style={{ marginBottom: theme.SIZES.BASE * 1.875, paddingHorizontal: theme.SIZES.BASE * 2 }}>
         The place where Students meets their Professionals to teach them how to succeed 
         give it a try ! 
        </Text>
        <Button size="large" color="transparent" round onPress={() => navigation.navigate('Login')}>
          Get Started
        </Button>
      </Block>
      <Block flex style={{ marginBottom: -Constants.statusBarHeight * 2 }}>
        <Image source={iphoneImage} style={{ width }} />
      </Block>
    </Block>
  </Block>




  )}


  const styles = StyleSheet.create({
    backgroundImage: {
      flex: 1,
      width: '100%',
    },
    backgroundGradient: {
      position: 'absolute',
      top: 0,
      bottom: 0,
      right: 0,
      left: 0,
      zIndex: 0,
    },
  
    absolute: {
      position: 'absolute',
      bottom: 0,
      left: 0,
      right: 0,
    },
    gradient: {
      position: 'absolute',
      bottom: 0,
      left: 0,
      right: 0,
      height: 90,
    },
    articleSummary: {
      paddingLeft: 20,
      paddingBottom: 20
    },
    container: {
      paddingHorizontal: theme.SIZES.BASE,
    },
  });

