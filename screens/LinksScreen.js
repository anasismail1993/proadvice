
import React,{useEffect} from 'react';
import { Image,SafeAreaView, TouchableHighlight, View,Text,FlatList,StyleSheet,Dimensions } from 'react-native';
import theme from "../theme";
import { useDispatch, useSelector } from "react-redux";
import { getChat} from "../store/actions";
import Firebase from "../utility/Firebase";




const { width } = Dimensions.get('window')



import { MonoText } from '../components/StyledText';

export default function LinksScreen({ navigation }) {
  const dispatch = useDispatch();
  const ChatListe = useSelector(
    (state) => state.chat.ChatListe
  );
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      dispatch(getChat(Firebase.auth().currentUser.uid));
    });

    return unsubscribe;

   
  }, [navigation]);
  return (
 

 
    <SafeAreaView style={{ flex: 1, backgroundColor: "#F6F8F9" }}>
    <View style={{ flex: 1,marginTop: 40  }}>
     
          <Text style={{ fontSize: 24, fontWeight: "700", paddingHorizontal: 20,alignSelf:'center' }}>
            Messages
          </Text>


          
          <FlatList
      data={ChatListe}
      renderItem={({ item }) =>        
      <TouchableHighlight underlayColor='white' onPress={() => navigation.navigate('Chat',{idConv:item.id,name:item.userName})}>
      <View style={styles.categoriesItemContainer}>
        <Image style={styles.categoriesPhoto}  source={{uri:item.avatar}} />
  <Text style={{marginLeft:10, fontSize: 20}}> {item.userName}</Text>
      </View>
    </TouchableHighlight>}
      keyExtractor={item => item.id}
    />
   

       
      
    </View>
  </SafeAreaView>






          









 

  )}
  const styles = StyleSheet.create({
    header: {
      backgroundColor: theme.COLORS.WHITE,
      borderTopLeftRadius: theme.SIZES.BASE * 2,
      borderTopRightRadius: theme.SIZES.BASE * 2,
      paddingVertical: theme.SIZES.BASE * 2,
      paddingHorizontal: theme.SIZES.BASE * 1.5,
      width,
    },
    categoriesItemContainer: {
      flex: 1,
      margin: 10,
      justifyContent: 'flex-start',
      alignItems: 'center',
      height: 80,
      borderColor: '#cccccc',
      borderWidth: 0.5,
      borderRadius: 10,
      flexDirection:'row'
    },
    categoriesPhoto: {
      width: 60,
      height: 60,
      marginLeft:5,
      borderRadius: 50,
      borderBottomLeftRadius: 50,
      borderBottomRightRadius: 50,
      shadowColor: 'blue',
      shadowOffset: {
        width: 0,
        height: 3
      },
      shadowRadius: 5,
      shadowOpacity: 1.0,
      
    },
    categoriesName: {
      flex: 1,
      fontSize: 20,
      fontWeight: 'bold',
      textAlign: 'center',
      color: '#333333',
      marginTop: 8
    },
    categoriesInfo: {
      marginTop: 3,
      marginBottom: 5
    },
    stats: {
      borderWidth: 0,
      width: width - theme.SIZES.BASE * 2,
      height: theme.SIZES.BASE * 4,
      marginVertical: theme.SIZES.BASE * 0.875,
    },
    title: {
      justifyContent: 'center',
      paddingLeft: theme.SIZES.BASE / 2,
    },
    avatar: {
      width: theme.SIZES.BASE * 2.5,
      height: theme.SIZES.BASE * 2.5,
      borderRadius: theme.SIZES.BASE * 1.25,
    },
    middle: {
      justifyContent: 'center',
    },
    text: {
      fontSize: theme.SIZES.FONT * 0.875,
      lineHeight: theme.SIZES.FONT * 1.25,
    },
  });