import React, { useState } from "react";
import { Alert, Dimensions, KeyboardAvoidingView, Image,StyleSheet,View} from 'react-native';
import {Block, Button, Input, Text} from 'galio-framework';
import theme from '../theme';
import {signInWithEmailAndPassword} from '../store/actions';
import { useDispatch } from 'react-redux'


const { height, width } = Dimensions.get('window');
const iphoneImage = require('../assets/login.jpg');
export default function LoginScreen({ navigation }) {

  const [password, setPassword] = useState("");
  const [mail, setMail]= useState("");
  const dispatch = useDispatch();
    return (
        <Block safe flex style={{ backgroundColor: theme.COLORS.WHITE }}>
                        


<KeyboardAvoidingView style={styles.container} behavior="height" enabled>
<Block flex center style={{ marginTop: theme.SIZES.BASE * 1.875, marginBottom: height * 0.1 ,width:'100%'}}>
<Image source={iphoneImage} style={{ width:300,height:250,resizeMode:'contain' }} />
    
          </Block>
          <Block flex={2} center space="evenly">
            <Block flex={2}>
            <Input rounded type="email-address" value={mail} onChangeText={mail=> setMail( mail )} placeholder="Email" autoCapitalize="none" style={{ width: width * 0.9 }} />
            <Input rounded password placeholder="Password" style={{ width: width * 0.9 }} onChangeText={password => setPassword(password)} />
            <Text  color={theme.COLORS.ERROR}  size={theme.SIZES.FONT * 0.75}  onPress={() => Alert.alert('Not implemented')}  style={{ alignSelf: 'flex-end', lineHeight: theme.SIZES.FONT * 2 }}>  Forgot your password?  </Text>
            </Block>

                <Block flex middle>
              <Button  round  color="#5373b8"  onPress={() => dispatch(signInWithEmailAndPassword(mail,password))} > Sign in </Button>
            
              <Button color="transparent" shadowless >
                <Text center color={theme.COLORS.ERROR} size={theme.SIZES.FONT * 0.75} onPress={() => navigation.navigate('RegisterChoice')}>
                  {"Don't have an account? Sign Up"}
                </Text>
              </Button>
            </Block>
                </Block>


</KeyboardAvoidingView>
</Block>

    )}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
        paddingTop: theme.SIZES.BASE * 0.3,
        paddingHorizontal: theme.SIZES.BASE,
        backgroundColor: theme.COLORS.WHITE,
    }

});
