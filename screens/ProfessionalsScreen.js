import React, { useState, useEffect, Component } from "react";
import {
  View,
  Text,
  TouchableHighlight,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  FlatList,
  Image,
  Dimensions
} from "react-native";
import { Block, Card, Icon, NavBar, Button, Toast } from "galio-framework";
const { width, height } = Dimensions.get("window");
import theme from "../theme";
import "firebase/firestore";
import { getProfessionals, TrysetRequest } from "../store/actions";
import { useDispatch, useSelector } from "react-redux";
import Modal from "react-native-modal";
import Firebase from "../utility/Firebase";

export default function ProfessionalsScreen({ navigation, route }) {
  const dispatch = useDispatch();
  const [ModalShow, setModalShow] = useState(false);
  const { companyId } = route.params;
  const professionalsListe = useSelector(
    state => state.Professionals.ProfessionalsListe
  );
  const isLoading = useSelector(state => state.ui.isLoading);
  const RequestResult = useSelector(state => state.Request.RequestResult);
  const [ProId, setProId] = useState("");
  const [isShow, setShow] = useState(true);

  useEffect(() => {
    dispatch(getProfessionals(companyId,Firebase.auth().currentUser.uid));
    setModalShow(false);
  }, [RequestResult]);
  const contactHundler = id => {
    setModalShow(true);
    setProId(id);
  };
  const cancelHundler = () => {
    setModalShow(false);
    setProId("");
  };
  const sendRequest = () => {
    dispatch(TrysetRequest(ProId, Firebase.auth().currentUser.uid, 0));
  };
  return (
    <View style={{ flex: 1, paddingTop: 20 }}>
       <Toast
                  isShow={true}
                  color="success"
                  round="true"
                >
                  Request Sent !
                </Toast>
      <Text
        style={{
          fontSize: 24,
          fontWeight: "700",
          paddingHorizontal: 20,
          alignSelf: "center"
        }}
      >
        Professionals
      </Text>
      {!isLoading && (
        <FlatList
          data={professionalsListe}
          extraData={professionalsListe}
          ListEmptyComponent={() => (
            <View>
              <Image
                style={{ width: 200, height: 200, alignSelf: "center" }}
                source={require("../assets/empty.jpg")}
              />
              <Text
                style={{
                  fontSize: 24,
                  fontWeight: "700",
                  paddingHorizontal: 20,
                  alignSelf: "center"
                }}
              >
                {" "}
                Empty! for now{" "}
              </Text>
            </View>
          )}
          renderItem={({ item }) => (
            <View
              style={{
                width: "95%",
                elevation: 8,
                overflow: "hidden",
                height: 320,
                backgroundColor: "white",
                marginBottom: 20,
                marginTop: 1,
                alignSelf: "center",
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
                borderBottomLeftRadius: 20,
                borderBottomRightRadius: 20
              }}
            >
              <Image
                style={{
                  width: 150,
                  height: 150,
                  alignSelf: "center",
                  borderRadius: 200 / 2
                }}
                source={{uri:item.avatar}}
              />
              <Text
                style={{
                  fontSize: 24,
                  paddingHorizontal: 20,
                  fontFamily: "bold"
                }}
              >
                {item.fullName}
              </Text>

              <Text
                style={{
                  fontSize: 12,
                  paddingHorizontal: 20,
                  color: "#5B64F6",
                  fontFamily: "regular"
                }}
              >
                {item.mail}
              </Text>
              <Text
                ellipsizeMode={"tail"}
                numberOfLines={4}
                style={{
                  fontSize: 13,
                  fontWeight: "700",
                  paddingHorizontal: 20,
                  color: "#939597",
                  marginTop: 5
                }}
              >
                {item.role}
              </Text>
              <Button
                size="small"
                round
                color="#5373b8"
                style={{ marginTop: 20, alignSelf: "center" }}
                onPress={() => contactHundler(item.id)}
                disabled={item.Requeststatus==1 || item.Requeststatus==0 ? true:false}
              >
                Contact
              </Button>
            </View>
          )}
        />
      )}
      <View>
        <Modal isVisible={ModalShow}>
          <View
            style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
          >
            <View
              style={{
                width: "100%",
                height: "40%",
                backgroundColor: "white",
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
                borderBottomLeftRadius: 20,
                borderBottomRightRadius: 20,
                padding: 5
              }}
            >
              <Text
                style={{
                  fontSize: 24,
                  fontWeight: "700",
                  paddingHorizontal: 20,
                  alignSelf: "center",
                  marginTop: 20
                }}
              >
                Send Contact Request ?
              </Text>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  marginTop: 50
                }}
              >
                <Button
                  round
                  size="small"
                  color="success"
                  style={{ width: "40%" }}
                  onPress={() => sendRequest()}
                >
                  Contact
                </Button>

                <Button
                  round
                  size="small"
                  color="error"
                  style={{ width: "40%" }}
                  onPress={() => cancelHundler()}
                >
                  cancel
                </Button>
              </View>
            </View>
          </View>
        </Modal>
      </View>

      <View style>
     
      </View>
    </View>
  );
}
