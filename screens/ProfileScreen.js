import React from 'react';
import {
  Image,
  StatusBar,
  StyleSheet,
  ScrollView,
  Dimensions,
  View,
  TouchableOpacity} from 'react-native';

import Constants from 'expo-constants';
import { useDispatch,useSelector } from 'react-redux'

const { statusBarHeight } = Constants;

// galio components
import {
  Block, Card, Text, Icon, NavBar,Button
} from 'galio-framework';
import theme from '../theme';

const { width, height } = Dimensions.get('screen');


export default function ProfileScreen({ navigation }) {
  const user = useSelector((state)=> state.auth.user);

    return (
        <Block>
                  <ScrollView>

        <StatusBar barStyle="light-content" />
    
        <Image
          source={{uri:user.avatar}}
          resizeMode="cover"
          style={{
            width,
            height: height * 0.55,
          }}
        />
    
        <Block center style={{ marginTop: -theme.SIZES.BASE * 2 }}>
          <Block flex style={styles.header}>
            <View style={{flexDirection: "row",justifyContent: "space-between"}}>
              <View>
              <Text size={theme.SIZES.BASE * 1.875} >{user.fullName}</Text>
             <Text muted t size={theme.SIZES.BASE * 0.875} style={{  fontWeight: '500' }}>
                {user.role} </Text>
              </View>
              <View> 
                <Button onlyIcon icon="edit" iconFamily="antdesign" iconSize={30} color="transparent" iconColor="black" style={{ width: 40, height: 40 }} onPress={() => navigation.navigate('UpdateProfile')}>Update Profile</Button>
              </View>
              <View> 
                <Button onlyIcon icon="setting" iconFamily="antdesign" iconSize={30} color="transparent" iconColor="black" style={{ width: 40, height: 40 }} onPress={() => navigation.navigate('Settings')}>Update Profile</Button>
              </View>
              </View>
    
            <Block center>
              <Card
                borderless
                style={styles.stats}
                title={user.companyTitle}
                caption={user.companyAdress}
                avatar={user.companyAvatar}
                location={(
                  <Block row right>
                    <Block row middle style={{ marginHorizontal: theme.SIZES.BASE }}>
                    
                      <Text
                        p
                        color={theme.COLORS.MUTED}
                        size={theme.SIZES.FONT * 0.875}
                        style={{ marginLeft: theme.SIZES.BASE * 0.25 }}
                      >
                        
                      </Text>
                    </Block>
                    <Block row middle>
                    
                      <Text
                        p
                        color={theme.COLORS.MUTED}
                        size={theme.SIZES.FONT * 0.875}
                        style={{ marginLeft: theme.SIZES.BASE * 0.25 }}
                      >
                       
                      </Text>
                    </Block>
                  </Block>
                )}
              />
            </Block>
            <ScrollView>
              <Text style={styles.text}>
            {user.description}
              </Text>
              
            </ScrollView>
          </Block>
        </Block>
        </ScrollView>

      </Block>
    )}
    
const styles = StyleSheet.create({
    header: {
      backgroundColor: theme.COLORS.WHITE,
      borderTopLeftRadius: theme.SIZES.BASE * 2,
      borderTopRightRadius: theme.SIZES.BASE * 2,
      paddingVertical: theme.SIZES.BASE * 2,
      paddingHorizontal: theme.SIZES.BASE * 1.5,
      width,
    },
    navbar: {
      top: statusBarHeight,
      left: 0,
      right: 0,
      zIndex: 9999,
      position: 'absolute',
    },
    stats: {
      borderWidth: 0,
      width: width - theme.SIZES.BASE * 2,
      height: theme.SIZES.BASE * 4,
      marginVertical: theme.SIZES.BASE * 0.875,
    },
    title: {
      justifyContent: 'center',
      paddingLeft: theme.SIZES.BASE / 2,
    },
    avatar: {
      width: theme.SIZES.BASE * 2.5,
      height: theme.SIZES.BASE * 2.5,
      borderRadius: theme.SIZES.BASE * 1.25,
    },
    middle: {
      justifyContent: 'center',
    },
    text: {
      fontSize: theme.SIZES.FONT * 0.875,
      lineHeight: theme.SIZES.FONT * 1.25,
    },
  });
  
  