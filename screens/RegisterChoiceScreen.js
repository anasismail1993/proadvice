import React, { useState } from "react";
import {Image, Platform, StyleSheet,Dimensions, TouchableOpacity, View , StatusBar} from 'react-native';
import Carousel from "react-native-carousel-control";

import {useTheme} from '@react-navigation/native';
import theme from '../theme';
import { Block, Button, NavBar, Text} from 'galio-framework';
import { LinearGradient } from 'expo-linear-gradient';
import Constants from 'expo-constants';


  

export default function RegisterChoiceScreen({ navigation })  {
  const {colors} = useTheme();
  const iphoneImage = require('../assets/iphone.png');
  const { width } = Dimensions.get('screen');
  return (
    <Block flex>
    <StatusBar hidden={true} barStyle="light-content" />
    <Block style={styles.navbar}>
      <NavBar
        transparent
        leftIconColor={theme.COLORS.WHITE}
      />
    </Block>
    <LinearGradient
      colors={['#2F297B', '#0D3A32']}
      end={[0.5, 0.9]}
      style={styles.backgroundGradient}
    />
    <Block flex center style={styles.container}>
      <Block flex middle >
      <Text center size={theme.SIZES.FONT * 2.375} color={theme.COLORS.WHITE} style={{ marginBottom: theme.SIZES.BASE }}>
          ProAdvice
        </Text>
        <Text center size={theme.SIZES.FONT * 0.875} color={theme.COLORS.WHITE} style={{ marginBottom: theme.SIZES.BASE * 1.875, paddingHorizontal: theme.SIZES.BASE * 2 }}>
         Before We Continue please tell us who you're ?
        </Text>
        <Button size="large" color="transparent" round onPress={() => navigation.navigate('Register')}>
          Student 
        </Button>
         <Button style={{marginTop:20}} size="large" color="transparent" round onPress={() => navigation.navigate('RegisterAdmin')}>
          Professional 
        </Button>
      </Block>

     
    </Block>
  </Block>




  )}

   
  const styles = StyleSheet.create({
    backgroundImage: {
      flex: 1,
      width: '100%',
    },
    backgroundGradient: {
      position: 'absolute',
      top: 0,
      bottom: 0,
      right: 0,
      left: 0,
      zIndex: 0,
    },
  
    absolute: {
      position: 'absolute',
      bottom: 0,
      left: 0,
      right: 0,
    },
    gradient: {
      position: 'absolute',
      bottom: 0,
      left: 0,
      right: 0,
      height: 90,
    },
    articleSummary: {
      paddingLeft: 20,
      paddingBottom: 20
    },
    container: {
      paddingHorizontal: theme.SIZES.BASE,
    },
  });


    
