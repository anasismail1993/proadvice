import React, { useState,useEffect } from "react";
import {
   View,TouchableOpacity, Image, Dimensions, KeyboardAvoidingView,Picker, StyleSheet, Platform,
} from 'react-native';
import {Block, Button, Input, Text,NavBar} from 'galio-framework';
import theme from '../theme';
import 'firebase/firestore';
import firebase from '../utility/Firebase'
import Cryptojs from "react-native-crypto-js"
const iphoneImage = require('../assets/signUp.jpg');

const { height, width } = Dimensions.get('window');
import PassMeter from "react-native-passmeter";
import { firestore } from "firebase";
const MAX_LEN = 15,
  MIN_LEN = 6,
  PASS_LABELS = ["Too Short", "Weak", "Normal", "Strong", "Secure"];


  
const dbh = firebase.firestore()

//async const getMarker=()=> {
  //  const snapshot = await firebase.firestore().collection('events').get()
    //return console.warn (snapshot.docs.map(doc => doc.data()));
// }
export default function RegisterProfessionalScreen({ navigation }) {
    const [password, setPassword] = useState("");
    const [fullName, setFullName]= useState("");
    const [mail, setMail]= useState("");
    const [selectedValue, setSelectedValue] = useState("");
    const [companieList, setCompanieList]= useState([]);
    
   useEffect(() => {        
        dbh.collection('Companies').onSnapshot(documentSnapshot => {
            documentSnapshot.forEach(doc => {
                setCompanieList(companieList=>[...companieList,{
                    title:doc.data().title,
                    id:doc.id
                }])
            })
          });
        
      },[]);
    return (
        <Block safe flex style={{ backgroundColor: theme.COLORS.WHITE }}>
            <KeyboardAvoidingView style={styles.container} behavior="height" enabled>
                
                <Block flex center>
                <Image source={iphoneImage} style={{ width:300,height:250,resizeMode:'contain' }} />

                </Block>
                <Block flex={2} center space="evenly">
                    <Block flex={2}>
                        
                        <Input rounded value={fullName} onChangeText={fullName=> setFullName( fullName )} 
                        type="default" placeholder="Full Name" autoCapitalize="none" style={{ width: width * 0.9 }}/>

                        <Input rounded value={mail} onChangeText={mail=> setMail( mail )} 
                        type="email-address" placeholder="Email" autoCapitalize="none" style={{ width: width * 0.9 }}/>

                        <Input rounded  password  placeholder="Password"  style={{ width: width * 0.9 }}  
                        onChangeText={password => setPassword(password)}  />

                        <PassMeter showLabels  password={password}  maxLength={MAX_LEN} minLength={MIN_LEN}  labels={PASS_LABELS}/>
                        <Picker
        selectedValue={selectedValue} style={{ height: 50, width: width * 0.9}} onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}>
{ companieList.map((item,index)=>(
    <Picker.Item label={item.title} value={item.id} />
)) }      
    
      </Picker>

                    </Block>
                    <Block flex middle>
                        <Button round color="#3B5998" onPress={() => register(fullName,password,mail,navigation,selectedValue)}>
                            Join us
                        </Button>
                     
                    </Block>
                </Block>


            </KeyboardAvoidingView>
        </Block>

    )}
    const register=(fullName,password,mail,navigation,companyId)=>{
  
        if (fullName!="" && mail!="" && password!="") 
        firebase.auth().createUserWithEmailAndPassword(mail,password).then(
            ()=>{

          
        dbh.collection("users").doc(firebase.auth().currentUser.uid).set({
            fullName,
            mail,
            password:Cryptojs.AES.encrypt(password,'0000').toString(),
            company:companyId,
            role:"pro"

           })
           .then(function(){
                navigation.reset({routes:[{ name:'Home'}]})
           })
        }
        )

        }
    
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
        paddingTop: theme.SIZES.BASE * 0.3,
        paddingHorizontal: theme.SIZES.BASE,
        backgroundColor: theme.COLORS.WHITE,
    },
    social: {
        width: theme.SIZES.BASE * 3.5,
        height: theme.SIZES.BASE * 3.5,
        borderRadius: theme.SIZES.BASE * 1.75,
        justifyContent: 'center',
    },
});

   
    
