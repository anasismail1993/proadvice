import React, { useState, useEffect } from "react";
import {
  Alert,
  Dimensions,
  KeyboardAvoidingView,
  Image,
  StyleSheet,
  View,
  SafeAreaView,
  FlatList,
  TouchableHighlight
} from "react-native";
import { Block, Button, Input, Text } from "galio-framework";
import theme from "../theme";
import { useDispatch, useSelector } from "react-redux";
import * as ImagePicker from "expo-image-picker";
import { GetListRequest, TryAccept, TryDecline } from "../store/actions";
const { height, width } = Dimensions.get("window");
const iphoneImage = require("../assets/login.jpg");
import firebase from "../utility/Firebase";

export default function RequestScreen({ navigation }) {
  const dispatch = useDispatch();

  const RequestListe = useSelector(state => state.Request.RequestListe);
  const acceptRequestResult = useSelector(
    state => state.Request.acceptRequestResult
  );
  const isLoading = useSelector(state => state.ui.isLoading);
  useEffect(() => {
    dispatch(GetListRequest(firebase.auth().currentUser.uid));
  }, [acceptRequestResult]);
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#F6F8F9" }}>
      <View style={{ flex: 1, marginTop: 40 }}>
        <Text
          style={{
            fontSize: 24,
            fontWeight: "700",
            paddingHorizontal: 20,
            alignSelf: "center"
          }}
        >
          Requests
        </Text>

        {!isLoading && (
          <FlatList
            data={RequestListe}
            keyExtractor={item => item.id}
            renderItem={({ item }) => (
              <TouchableHighlight
                underlayColor="white"
                onPress={() => navigation.navigate("Chat")}
              >
                <View style={styles.categoriesItemContainer}>
                  <Image
                    style={styles.categoriesPhoto}
                    source={{ uri: item.avatar }}
                  />
                  <Text style={{ marginLeft: 10, fontSize: 20 }}>
                    {" "}
                    {item.name}
                  </Text>
                  <Button
                    round
                    size="small"
                    style={{ width: 80 }}
                    color="success"
                    onPress={() =>
                      dispatch(
                        TryAccept(item.id, item.idSender, item.idReciever)
                      )
                    }
                  >
                    Accept
                  </Button>
                  <Button
                    round
                    size="small"
                    style={{ width: 80 }}
                    color="error"
                    onPress={() => dispatch(TryDecline(item.id))}
                  >
                    Decline
                  </Button>
                </View>
              </TouchableHighlight>
            )}
            keyExtractor={item => item.id}
          />
        )}
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: theme.COLORS.WHITE,
    borderTopLeftRadius: theme.SIZES.BASE * 2,
    borderTopRightRadius: theme.SIZES.BASE * 2,
    paddingVertical: theme.SIZES.BASE * 2,
    paddingHorizontal: theme.SIZES.BASE * 1.5,
    width
  },
  categoriesItemContainer: {
    flex: 1,
    margin: 10,
    justifyContent: "space-between",
    alignItems: "center",
    height: 80,
    borderColor: "#cccccc",
    borderWidth: 0.5,
    borderRadius: 10,
    flexDirection: "row"
  },
  categoriesPhoto: {
    width: 60,
    height: 60,
    marginLeft: 5,
    borderRadius: 50,
    borderBottomLeftRadius: 50,
    borderBottomRightRadius: 50,
    shadowColor: "blue",
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: 1.0
  },
  categoriesName: {
    flex: 1,
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center",
    color: "#333333",
    marginTop: 8
  },
  categoriesInfo: {
    marginTop: 3,
    marginBottom: 5
  },
  stats: {
    borderWidth: 0,
    width: width - theme.SIZES.BASE * 2,
    height: theme.SIZES.BASE * 4,
    marginVertical: theme.SIZES.BASE * 0.875
  },
  title: {
    justifyContent: "center",
    paddingLeft: theme.SIZES.BASE / 2
  },
  avatar: {
    width: theme.SIZES.BASE * 2.5,
    height: theme.SIZES.BASE * 2.5,
    borderRadius: theme.SIZES.BASE * 1.25
  },
  middle: {
    justifyContent: "center"
  },
  text: {
    fontSize: theme.SIZES.FONT * 0.875,
    lineHeight: theme.SIZES.FONT * 1.25
  }
});
