import React from "react";
import {
  View,
  Switch,
} from "react-native";
import { Block, Card, Text, Button, NavBar } from "galio-framework";
import "firebase/firestore";
import firebase from "../utility/Firebase";

export default function SettingsScreen({ navigation }) {
  return (
    <View style={{ flex: 1, paddingTop: 20 }}>
      <NavBar title="Settings" />
      <View
        style={{
          width: "85%",
          alignSelf: "center",
          height: 1,
          backgroundColor: "#D8D8D8",
          marginTop: 8,
        }}
      ></View>
      <View style={{ flexDirection: "row", justifyContent: "space-between",padding:10 }}>
        <Text
          style={{
            fontSize: 14,
            fontWeight: "700",
            alignSelf: "center",
            color: "grey",
          }}
        >
       
          Notifications
        </Text>

        <Switch onValueChange={() => this.toggleSwitch("switch-1")} />
      </View>
      <View
        style={{
          width: "85%",
          alignSelf: "center",
          height: 1,
          backgroundColor: "#D8D8D8",
        }}
      ></View>
      <View style={{ flexDirection: "row", justifyContent: "space-between",paddingLeft:10}}>
        <Text
          style={{
            fontSize: 14,
            fontWeight: "700",
            alignSelf: "center",
            color: "grey",
          }}
        >
       
          Logout
        </Text>
        <Button
        onlyIcon
        icon="logout"
        iconFamily="antdesign"
        iconSize={30}
        color="transparent"
        style={{ width: 40, height: 40 }}
        size="small"
        onPress={() => firebase.auth().signOut()}
        style={{ width: 80 }}
      >
        logout
      </Button>
        </View>
        <View
        style={{
          width: "85%",
          alignSelf: "center",
          height: 1,
          backgroundColor: "#D8D8D8",
        }}
      ></View>
     
    </View>
  );
}
