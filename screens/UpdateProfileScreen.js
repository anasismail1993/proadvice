import React, { useState, useEffect } from "react";
import {
  Alert,
  Dimensions,
  KeyboardAvoidingView,
  Image,
  StyleSheet,
  View,
  ActivityIndicator,
  TextInput,
} from "react-native";
import { Block, Button, Input, Text, NavBar } from "galio-framework";
import theme from "../theme";
import { useDispatch, useSelector } from "react-redux";
import * as ImagePicker from "expo-image-picker";
import {
  TryUploadFile,
  updateImage,
  updateDescription,
} from "../store/actions";
const { height, width } = Dimensions.get("window");
const iphoneImage = require("../assets/login.jpg");
import firebase from "../utility/Firebase";
import { ScrollView } from "react-native-gesture-handler";
export default function UpdateProfileScreen({ navigation }) {
  const [description, setDescription] = useState(useSelector((state) => state.auth.user.description));
  const [password, setPassword] = useState("");
  const dispatch = useDispatch();
  const [image, setImage] = useState(null);
  const user = useSelector((state) => state.auth.user);
  const urlImage = useSelector((state) => state.Upload.url);
  const isLoading = useSelector((state) => state.ui.isLoading);


  useEffect(() => {
    
    (async () => {
      if (Constants.platform.ios) {
        const {
          status,
        } = await ImagePicker.requestCameraRollPermissionsAsync();
        if (status !== "granted") {
          alert("Sorry, we need camera roll permissions to make this work!");
        }
      }
    })();
    if (urlImage !== "") {
      dispatch(updateImage(firebase.auth().currentUser.uid, urlImage));
    }
  }, [urlImage]);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setImage(result.uri);
    }
  };
  const updateImageHandler = () => {
    if (image != null) {
      dispatch(TryUploadFile(image, firebase.auth().currentUser.uid));
    } else {
    }
  };

  const updateDescriptionHandler = () => {
    console.warn("clicked");
    dispatch(updateDescription(firebase.auth().currentUser.uid, description));
  };

  return (
    <Block safe flex style={{ backgroundColor: theme.COLORS.WHITE }}>
      <NavBar
        title="Update Profile"
        left
        back
        onLeftPress={() => {
          navigation.navigate("Profile");
        }}
        titleStyle={{ fontSize: 14, fontWeight: "700" }}
      />

      <KeyboardAvoidingView style={styles.container} behavior="height" enabled>
      <ScrollView>
        <View
          style={{
            width: "85%",
            alignSelf: "center",
            height: 1,
            backgroundColor: "#D8D8D8",
            marginTop: 8,
          }}
        ></View>
        <Text
          style={{
            fontSize: 14,
            fontWeight: "700",
            alignSelf: "center",
            marginTop: 20,
            color: "grey",
          }}
        >
          Update Profile Picture
        </Text>

        <View
          style={{
            alignItems: "center",
            justifyContent: "center",
            marginTop: 20,
          }}
        >
          {image && (
            <Image
              source={{ uri: image }}
              style={{ width: 200, height: 200, marginBottom: 10 }}
            />
          )}
          <Button
            onlyIcon
            icon="upload"
            iconFamily="antdesign"
            onPress={() => pickImage()}
            iconSize={20}
            color="transparent"
            style={{ width: 40, height: 40 }}
            size="small"
            style={{ width: 80 }}
          >
            logout
          </Button>

          {!isLoading ? (
            <Button
              title="Pick an image from camera roll"
              onPress={() => updateImageHandler()}
              round
              size="small"
              color="#5373b8"
            >
            
              update Image
            </Button>
          ) : (
            <ActivityIndicator size="large" color="#0000ff" />
          )}
        </View>
        <View
          style={{
            width: "85%",
            alignSelf: "center",
            height: 1,
            backgroundColor: "#D8D8D8",
            marginTop: 30,
          }}
        ></View>
        <Text
          style={{
            fontSize: 14,
            fontWeight: "700",
            alignSelf: "center",
            marginTop: 20,
            color: "grey",
          }}
        >
          Update Password
        </Text>
        <View style={{ alignItems: "center" }}>
          <Input
            placeholder="Insert Old Password .."
            right
            icon="lock1"
            family="antdesign"
            iconSize={14}
            iconColor="red"
            style={{ width: width * 0.9 }}
            onChangeText={(password) => setPassword(password)}
          />
          <Input
            placeholder="Insert New Password .."
            right
            icon="Safety"
            family="antdesign"
            iconSize={14}
            iconColor="red"
            style={{ width: width * 0.9 }}
            onChangeText={(password) => setPassword(password)}
          />
          <Input
            placeholder="Retype Password .."
            right
            icon="Safety"
            family="antdesign"
            iconSize={14}
            iconColor="red"
            style={{ width: width * 0.9 }}
            onChangeText={(password) => setPassword(password)}
          />
          <Button
            Button
            round
            uppercase
            color="error"
            size="small"
            style={{ marginTop: 20 }}
          >
           
            Update Password
          </Button>
          <View
            style={{
              width: "85%",
              alignSelf: "center",
              height: 1,
              backgroundColor: "#D8D8D8",
              marginTop: 30,
            }}
          ></View>
          <Text
            style={{
              fontSize: 14,
              fontWeight: "700",
              alignSelf: "center",
              marginTop: 20,
              color: "grey",
            }}
          >
            Update Description
          </Text>
          <TextInput
            clearButtonMode="always"
            multiline={true}
            numberOfLines={4}
            rounded
            placeholder="Update Description"
            style={{ width: width * 0.9 }}
            onChangeText={(description) => setDescription(description)}
            value={description}
          />
          <Button
            round
            color="#5373b8"
            style={{ marginTop: 20 }}
            onPress={() => updateDescriptionHandler()}
          >
            
            Update Description
          </Button>
        </View>

     
        </ScrollView>
      </KeyboardAvoidingView>
    </Block>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-around",
    paddingTop: theme.SIZES.BASE * 0.3,
    paddingHorizontal: theme.SIZES.BASE,
    backgroundColor: theme.COLORS.WHITE,
  },
});
