import * as React from 'react';
import { Image } from 'react-native';
import Onboarding from 'react-native-onboarding-swiper';
import { useTheme } from '@react-navigation/native'


export default function WelcomeScreen({ navigation }) {
    const {colors}=useTheme()
    return (
      <Onboarding
      onDone={() => navigation.navigate('Intro')}
      onSkip={() => navigation.navigate('Intro')}
      pages={[
        {
            backgroundColor: colors.background,
            image: <Image source={require('../assets/intro3.jpg')} style={{ width: 400, height: 400 }} />,
            title: 'Tired of searching for answers ?',
            subtitle: 'You came to the right place',

        },
          {
              backgroundColor: '#DCEDFD',
              image: <Image source={require('../assets/onboard.jpg')} style={{ width: 400, height: 400 }} />,
              title: 'The Road To Your Dream Job',
              subtitle: 'Always starts with great peace of tip from a PRO',

          },
          {
              backgroundColor: 'white',
              image: <Image source={require('../assets/intro2.jpg')} style={{ width: 400, height: 400 }}  />,
              title: 'Contact Your Dream Job Professionals',
              subtitle: 'Find them easily and contact them immediatly',

          },

      ]}

  />

  )}



