import 'firebase/firestore';
import firebase from '../../../utility/Firebase';
import { GET_FEATURED } from '../actionTypes';
import { startLoadingCompnay,stopLoadingCompany } from '../index';

const dbh = firebase.firestore()

export const getFeatured = () => {
  return (dispatch) => {
    dispatch(startLoadingCompnay());
    dbh.collection('Companies').onSnapshot(documentSnapshot => {
        dispatch(setFeaturedListe({
          id:documentSnapshot.docs[0].id,
           title:documentSnapshot.docs[0].data().title,
           des:documentSnapshot.docs[0].data().Description,
            adress:documentSnapshot.docs[0].data().Adress,
            img:documentSnapshot.docs[0].data().Image
        }))
        
    

      
    dispatch(stopLoadingCompany()); 
  })
}}

export const setFeaturedListe = (featuredListe)=>{
    return {
        type: GET_FEATURED,
        FeaturedListe:featuredListe
    }
}