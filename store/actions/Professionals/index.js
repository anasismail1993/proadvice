import "firebase/firestore";
import firebase from "../../../utility/Firebase";
import { GET_PROFESSIONALS } from "../actionTypes";
import { startLoading, stopLoading } from "../index";

const dbh = firebase.firestore();

export const getProfessionals = (id, iduser) => {
  return (dispatch) => {
    
    dispatch(startLoading());
    dbh
      .collection("users")
      .where("company", "==", id)
      .onSnapshot((documentSnapshot) => {
        const ProfessionalsListe = [];
        documentSnapshot.forEach((doc) => {
           dbh
            .collection("Requests").doc(`${doc.id}${iduser}`)
            .onSnapshot(async(request) => {
          // console.warn(request.docs[0].data())
          ProfessionalsListe.push({
            id: doc.id,
            fullName: doc.data().fullName,
            mail: doc.data().mail,
            role: doc.data().role,
            // Requeststatus:
            // request.docs[0] !== undefined &&
            //request.docs[0].data().status,
            avatar:
              doc.data().avatar && doc.data().avatar != ""
                ? doc.data().avatar
                : "https://firebasestorage.googleapis.com/v0/b/proadvice-6ac32.appspot.com/o/Users%2Fportrait.jpg?alt=media&token=090c17d4-b0d4-435d-84a7-6f5f1cb02329",
          });

           });
        });
        dispatch(setProfessionalsListe(ProfessionalsListe));
        dispatch(stopLoading());
      });
  };
};

export const setProfessionalsListe = (ProfessionalsListe) => {
  return {
    type: GET_PROFESSIONALS,
    professionalsListe: ProfessionalsListe,
  };
};
