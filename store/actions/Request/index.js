import firebase from "../../../utility/Firebase";
import {
  SET_REQUEST,
  SET_REQUEST_LIST,
  SET_ACCEPT_REQUEST
} from "../actionTypes";
import { startLoading, stopLoading } from "../index";

const dbh = firebase.firestore();

export const TrysetRequest = (idReciever, idSender, status) => {
  return dispatch => {
    dispatch(startLoading());
    dbh
      .collection("Requests")
      .doc()
      .set({
        users:[idSender,idReciever],
        idReciever,
        idSender,
        status
      })
      .then(() => {
        dispatch(setRequest(idReciever));
    dispatch(stopLoading());
      });

  };
};

export const GetListRequest = idReciever => {
  return dispatch => {
    
    dispatch(startLoading());

    dbh
      .collection("Requests")
      .where("idReciever", "==", idReciever)
      .where("status", "==", 0)
      .onSnapshot(documentSnapshot => {
        const RequestListe = [];
        documentSnapshot.forEach(doc => {
          dbh
            .collection("users")
            .doc(doc.data().idSender)
            .onSnapshot(userSender => {
              RequestListe.push({
                id: doc.id,
                status: doc.data().status,
                idReciever: doc.data().idReciever,
                idSender: doc.data().idSender,
                name: userSender.data().fullName,
                avatar: userSender.data().avatar
              });
              dispatch(SetListRequesr(RequestListe));
              dispatch(stopLoading());
            });
        });
      });
  };
};

export const TryAccept = (idRequest, idSender, idReciever) => {
  return dispatch => {
    dispatch(startLoading());
    dbh
      .collection("Requests")
      .doc(idRequest)
      .update({
        status: 1
      })
      .then(() => {
        dbh
          .collection("chat")
          .doc()
          .set({
            users: [idSender, idReciever]
          });
        dispatch(setAcceptRequestResult(idRequest));
        dispatch(stopLoading());
      });

    
  };
};

export const TryDecline = idRequest => {
  return dispatch => {
    dispatch(startLoading());
    dbh
      .collection("Requests")
      .doc(idRequest)
      .update({
        status: 2
      }).then(() => {
        dispatch(setAcceptRequestResult(idRequest));
      });

    dispatch(stopLoading());
  };
};

export const setRequest = requestResult => {
  return {
    type: SET_REQUEST,
    RequestResult: requestResult
  };
};
export const setAcceptRequestResult = acceptRequestResult => {
  return {
    type: SET_ACCEPT_REQUEST,
    acceptRequestResult: acceptRequestResult
  };
};

export const SetListRequesr = RequestListe => {
  return {
    type: SET_REQUEST_LIST,
    RequestListe: RequestListe
  };
};
