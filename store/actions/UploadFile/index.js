import 'firebase/firestore';
import firebase from '../../../utility/Firebase';
import { TRY_UPLOAD } from '../actionTypes';
import { startLoading, stopLoading } from '../index';
const dbh = firebase.firestore()

export const TryUploadFile =  (url,id) => {
  return async (dispatch) => {
    dispatch(startLoading());
    const response = await fetch(url);
    const blob = await response.blob();
firebase.storage().ref('Users').child(id).put(blob).then(() => 
firebase.storage().ref('Users').child(id).getDownloadURL().then((url) => {
    dispatch(setUploadURL(url)); 
})
).catch(error => console.warn(error));

   // dispatch(stopLoading()); 
  }
}

export const setUploadURL = (url)=>{
    return {
        type: TRY_UPLOAD,
        url:url
    }
}