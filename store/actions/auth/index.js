// import { startLoading, stopLoading, setUserInfo } from '../index';
// import auth from '@react-native-firebase/auth';
// import { LoginManager, AccessToken } from 'react-native-fbsdk';
// import firestore from '@react-native-firebase/firestore';
import "firebase/firestore";
import firebase from "../../../utility/Firebase";
import {
  TRY_AUTH,
  TRY_UPDATE_IMAGE,
  TRY_UPDATE_DESCRIPTION,
} from "../actionTypes";
import { startLoading, stopLoading, startLoadingUser,stopLoadingUser } from "../index";

const dbh = firebase.firestore();

export const signInWithEmailAndPassword = (mail, password) => {
  return (dispatch) => {
    dispatch(startLoading());
    firebase
      .auth()
      .signInWithEmailAndPassword(mail, password)
      .then(() => {
        dispatch(getUserInfo(firebase.auth().currentUser.uid));
      });
  };
};

export const updateImage = (id, avatar) => {
  return (dispatch) => {
    dispatch(startLoading());
    dbh
      .collection("users")
      .doc(id)
      .update({ avatar })
      .then(() => dispatch(setUpdateProfile(avatar)));

    dispatch(stopLoading());
  };
};
export const updateDescription = (id, description) => {
  return (dispatch) => {
    dispatch(startLoading());
    dbh
      .collection("users")
      .doc(id)
      .update({ description })
      .then(() => dispatch(setUpdateProfileDescription(description)));
    console.warn("desc updated ");

    dispatch(stopLoading());
  };
};

export const getUserInfo = (id) => {
  return (dispatch) => {
    dispatch(startLoadingUser());
    dbh
      .collection("users")
      .doc(id)
      .onSnapshot((documentSnapshot) => {
        if (documentSnapshot.data().company !==undefined) {
          dbh
            .collection("Companies")
            .doc(documentSnapshot.data().company)
            .onSnapshot((companysnapshot) => {
              dispatch(
                setUSerInfo({
                  fullName: documentSnapshot.data().fullName,
                  role: documentSnapshot.data().role,
                  comapnyId: documentSnapshot.data().company,
                  companyTitle: companysnapshot.data().title,
                  avatar:
                    documentSnapshot.data().avatar &&
                    documentSnapshot.data().avatar != ""
                      ? documentSnapshot.data().avatar
                      : "https://firebasestorage.googleapis.com/v0/b/proadvice-6ac32.appspot.com/o/Users%2Fportrait.jpg?alt=media&token=090c17d4-b0d4-435d-84a7-6f5f1cb02329",
                  companyAdress: companysnapshot.data().Adress,
                  companyAvatar: companysnapshot.data().Image.src,
                  description: documentSnapshot.data().description,
                })
              );
              dispatch(stopLoadingUser());
            });
        } else {
          dbh
          .collection("University")
          .doc(documentSnapshot.data().University)
          .onSnapshot((Universitysnapshot) => {
            
          dispatch(

            setUSerInfo({
              fullName: documentSnapshot.data().fullName,
              role: documentSnapshot.data().role,

              avatar:
                documentSnapshot.data().avatar &&
                documentSnapshot.data().avatar != ""
                  ? documentSnapshot.data().avatar
                  : "https://firebasestorage.googleapis.com/v0/b/proadvice-6ac32.appspot.com/o/Users%2Fportrait.jpg?alt=media&token=090c17d4-b0d4-435d-84a7-6f5f1cb02329",
              description: documentSnapshot.data().description,
              companyAdress: Universitysnapshot.data().Adress,
                  companyAvatar: Universitysnapshot.data().Image.src,
                  comapnyId: documentSnapshot.data().University,
                  companyTitle: Universitysnapshot.data().title,
            })
          );

          dispatch(stopLoadingUser());
        })
        }
      });
  };
};

export const setUpdateProfile = (url) => {
  return {
    type: TRY_UPDATE_IMAGE,
    avatar: url,
  };
};

export const setUpdateProfileDescription = (description) => {
  return {
    type: TRY_UPDATE_DESCRIPTION,
    description: description,
  };
};

export const setUSerInfo = (user) => {
  return {
    type: TRY_AUTH,
    user: user,
  };
};
