import "firebase/firestore";
import firebase from "../../../utility/Firebase";
import { GET_LIST_CONVERSATION,GET_LIST_MSG } from "../actionTypes";
import { startLoading, stopLoading } from "../index";

const dbh = firebase.firestore();
const dbr = firebase.database();

export const getChat = iduser => {
  return dispatch => {
    const ChatListe = [];
    dispatch(startLoading());
    dbh
      .collection("chat")
      .where("users", "array-contains", iduser)
      .onSnapshot(documentSnapshot => {
        documentSnapshot.forEach(doc => {
          const Sender = doc.data().users.filter(e => e != iduser);
          dbh
            .collection("users")
            .doc(Sender[0])
            .onSnapshot(userinfo => {
              ChatListe.push({
                id: doc.id,
                userName: userinfo.data().fullName,
                avatar: userinfo.data().avatar
              });
              dispatch(setChatListe(ChatListe));
            });
        });

        dispatch(stopLoading());
      });
  };
};
export const setSocketOff=(ref)=>{
return dispatch => {
  dbr.ref(ref).off();
}
}

export const addNewMessage = (message,ref,user) => {
  return dispatch => {
    dbr.ref(ref).push({
      user,
      message,
      date:new Date().toISOString()
    })
  };
};

export const getMessageConv = (ref) => {
  return dispatch =>{
    
dbr.ref(ref).on("value", snapshot => {
  dispatch(startLoading())
  const listMsg=[]
snapshot.forEach(msg =>{
  listMsg.push({
    idMsg:msg.key,
    msg:msg.val().message,
    idUser:msg.val().user,
    date:msg.val().date,
  })
})
  dispatch(setMsgListe(listMsg))
  dispatch(startLoading())
});
  }
}

export const setChatListe = ChatListe => {
  return {
    type: GET_LIST_CONVERSATION,
    ChatListe: ChatListe
  };
};
export const setMsgListe = listMsg => {
  return {
    type: GET_LIST_MSG,
    listMsg: listMsg
  };
};
