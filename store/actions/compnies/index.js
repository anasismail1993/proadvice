import 'firebase/firestore';
import firebase from '../../../utility/Firebase';
import { GET_COMPANIES,GET_ALL_COMPANIES  } from '../actionTypes';
import { startLoading, stopLoading } from '../index';

const dbh = firebase.firestore()

export const getCompanies = (id) => {
  return (dispatch) => {
      const companiesListe=[];
    dispatch(startLoading());
    dbh.collection('Companies').where('Domains','==',id).onSnapshot(documentSnapshot => {
        documentSnapshot.forEach(doc =>  {
            companiesListe.push({
                id:doc.id,
                title:doc.data().title,
                des:doc.data().Description,
                adress:doc.data().Adress,
                img:doc.data().Image
            })
        })
        dispatch(setCompaniesListe(companiesListe))
        dispatch(stopLoading()); 
    })

   
  }
}
export const getAllCompanies = () => {
    return (dispatch) => {
        const AllcompaniesListe=[];
      dispatch(startLoading());
      dbh.collection('Companies').onSnapshot(documentSnapshot => {
          documentSnapshot.forEach(doc =>  {
              companiesListe.push({
                  id:doc.id,
                  title:doc.data().title,
                  des:doc.data().Description,
                  adress:doc.data().Adress,
                  img:doc.data().Image
              })
          })
          dispatch(setAllCompaniesListe(AllcompaniesListe))
          dispatch(stopLoading()); 
      })
  
     
    }
  }

export const setCompaniesListe = (companiesListe)=>{
    return {
        type: GET_COMPANIES,
        CompaniesListe:companiesListe
    }

    
}

export const setAllCompaniesListe = (AllcompaniesListe)=>{
    return {
        type: GET_ALL_COMPANIES,
        AllCompaniesListe:AllcompaniesListe
    }

    
}