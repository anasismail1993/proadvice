import 'firebase/firestore';
import firebase from '../../../utility/Firebase';
import { GET_DOMAINS } from '../actionTypes';
import { startLoading, stopLoading } from '../index';

const dbh = firebase.firestore()

export const getDomains = () => {
  return (dispatch) => {
      
    dispatch(startLoading());
    let query = dbh.collection('Domains');
query.orderBy('title').onSnapshot(documentSnapshot => {
    const DomainListe=[];
    if(documentSnapshot.metadata.fromCache){

    }

    documentSnapshot.forEach(doc =>  {
    
DomainListe.push({
    id:doc.id,
    title:doc.data().title,
    image:doc.data().Image
})
    })
    dispatch(setDomainListe(DomainListe));
    dispatch(stopLoading()); 
})


   
  }
}

export const setDomainListe = (domainListe)=>{
    return {
        type: GET_DOMAINS,
        domainListe:domainListe
    }
}