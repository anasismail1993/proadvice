import 'firebase/firestore';
import firebase from '../../../utility/Firebase';
import { GET_DOMAINS_HOME } from '../actionTypes';
import { startLoadingDomainHome,stopLoadingDomainHome } from '../index';

const dbh = firebase.firestore()

export const getDomainsHome = () => {
  return (dispatch) => {
      const DomainListeHome=[];
    dispatch(startLoadingDomainHome());
dbh.collection('Domains').limit(6).onSnapshot(documentSnapshot => {
    documentSnapshot.forEach(doc =>  {
DomainListeHome.push({
    id:doc.id,
    title:doc.data().title,
    image:doc.data().Image
})
    })
    dispatch(setDomainListeHome(DomainListeHome));
    dispatch(stopLoadingDomainHome()); 
})


    
  }
}

export const setDomainListeHome = (domainListeHome)=>{
    return {
        type: GET_DOMAINS_HOME,
        domainListeHome:domainListeHome
    }
}