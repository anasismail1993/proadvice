import { SET_LOADING_TRUE,SET_LOADING_FALSE,SET_LOADING_COMPANY_TRUE,SET_LOADING_COMPANY_FALSE 
,SET_LOADING_DOMAIN_HOME_FALSE,SET_LOADING_DOMAIN_HOME_TRUE,SET_LOADING_USER_TRUE,SET_LOADING_USER_FALSE
} from "../actionTypes";

export const startLoading = () => {
    return {
        type: SET_LOADING_TRUE,
    };
  };

  export const stopLoading = () => {
    return {
        type: SET_LOADING_FALSE,
    };
  };

  
export const startLoadingCompnay = () => {
  return {
      type: SET_LOADING_COMPANY_TRUE,
  };
};

export const stopLoadingCompany = () => {
  return {
      type: SET_LOADING_COMPANY_FALSE,
  };
}
  
export const startLoadingDomainHome = () => {
  return {
      type: SET_LOADING_DOMAIN_HOME_TRUE,
  };
};

export const stopLoadingDomainHome = () => {
  return {
      type: SET_LOADING_DOMAIN_HOME_FALSE,
  };
}
export const startLoadingUser = () => {
  return {
      type: SET_LOADING_USER_TRUE,
  };
};

export const stopLoadingUser = () => {
  return {
      type: SET_LOADING_USER_FALSE,
  };
}