import { createStore, combineReducers, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import auth from "./reducer/auth";
import ui from "./reducer/ui";
import domain from "./reducer/domains";
import companies from "./reducer/companies";
import Professionals from "./reducer/Professionals";
import Featured from "./reducer/Featured";
import DomainHome from "./reducer/domainsHome";
import Upload from "./reducer/UploadFile";
import Request from './reducer/Request'
import chat from './reducer/chat'

const rootReducer = combineReducers({
  auth,
  ui,
  domain,
  companies,
  Professionals,
  Featured,
  DomainHome,
  Upload,
  Request,
  chat
});

export const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk))
);
