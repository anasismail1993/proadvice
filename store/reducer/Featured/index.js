import { GET_FEATURED } from '../../actions/actionTypes';

const initialState = {
  FeaturedListe: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_FEATURED:
      return {
        ...state,
        FeaturedListe: action.FeaturedListe,
      };
    default:
      return state;
  }
};

export default reducer;