import { GET_PROFESSIONALS } from '../../actions/actionTypes';

const initialState = {
  ProfessionalsListe: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_PROFESSIONALS:
      return {
        ...state,
        ProfessionalsListe: action.professionalsListe,
      };
    default:
      return state;
  }
};

export default reducer;