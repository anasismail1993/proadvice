import { SET_REQUEST,SET_REQUEST_LIST,SET_ACCEPT_REQUEST } from '../../actions/actionTypes';

const initialState = {
  RequestResult: null,
  acceptRequestResult:null,
  RequestListe: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_REQUEST:
      return {
        ...state,
        RequestResult: action.RequestResult,
      };
      case SET_ACCEPT_REQUEST:
      return {
        ...state,
        acceptRequestResult: action.acceptRequestResult,
      };
      case SET_REQUEST_LIST:
        return {
          ...state,
          RequestListe: action.RequestListe,
        };
    default:
      return state;
  }
};

export default reducer;