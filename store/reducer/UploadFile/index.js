import { TRY_UPLOAD } from '../../actions/actionTypes';

const initialState = {
  url: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case TRY_UPLOAD:
      return {
        ...state,
        url: action.url,
      };
    default:
      return state;
  }
};

export default reducer;