import { TRY_AUTH,TRY_UPDATE_IMAGE,TRY_UPDATE_DESCRIPTION } from '../../actions/actionTypes';

const initialState = {
  user: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case TRY_AUTH:
      return {
        ...state,
        user: action.user,
      };
      case TRY_UPDATE_IMAGE:
        return {
          ...state,
          user:{...state.user,avatar:action.avatar } ,
        };

        case TRY_UPDATE_DESCRIPTION:
          return {
            ...state,
            user:{...state.user,description:action.description } ,
          };
    default:
      return state;
  }
};

export default reducer;