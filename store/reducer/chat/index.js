import { GET_LIST_CONVERSATION,GET_LIST_MSG } from '../../actions/actionTypes';

const initialState = {
  ChatListe: [],
  listMsg:[]
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_LIST_CONVERSATION:
      return {
        ...state,
        ChatListe: action.ChatListe,
      };
      case GET_LIST_MSG :
      return {
        ...state,
        listMsg: action.listMsg,
      };
    default:
      return state;
  }
};

export default reducer;