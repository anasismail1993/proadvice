import { GET_COMPANIES } from '../../actions/actionTypes';
import { GET_ALL_COMPANIES } from '../../actions/actionTypes';

const initialState = {
  CompaniesListe: [],
  AllCompaniesListe: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_COMPANIES:
      return {
        ...state,
        CompaniesListe: action.CompaniesListe,
      };
      case GET_ALL_COMPANIES:
        return {
          ...state,
          AllCompaniesListe: action.AllCompaniesListe,
        };
    default:
      return state;
  }
};

export default reducer;