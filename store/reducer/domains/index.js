import { GET_DOMAINS } from '../../actions/actionTypes';

const initialState = {
  domainListe: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_DOMAINS:
      return {
        ...state,
        domainListe: action.domainListe,
      };
    default:
      return state;
  }
};

export default reducer;