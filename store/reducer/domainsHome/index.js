import { GET_DOMAINS_HOME } from '../../actions/actionTypes';

const initialState = {
  domainListeHome: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_DOMAINS_HOME:
      return {
        ...state,
        domainListeHome: action.domainListeHome,
      };
    default:
      return state;
  }
};

export default reducer;