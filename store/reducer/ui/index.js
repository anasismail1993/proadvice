import {SET_LOADING_TRUE, SET_LOADING_FALSE,SET_LOADING_COMPANY_FALSE,
  SET_LOADING_COMPANY_TRUE,SET_LOADING_DOMAIN_HOME_FALSE,SET_LOADING_DOMAIN_HOME_TRUE,SET_LOADING_USER_TRUE,SET_LOADING_USER_FALSE} from '../../actions/actionTypes';

const initialState = {
  isLoading: false,
  isLoadingCompany: false,
  isLoadingDomainHome:false,
  isLoadingUser:false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_LOADING_TRUE:
      return {
        ...state,
        isLoading: true,
      };
    case SET_LOADING_FALSE:
      return {
        ...state,
        isLoading: false,
      };
      case SET_LOADING_COMPANY_TRUE:
        return {
          ...state,
          isLoadingCompany: true,
        };
      case SET_LOADING_COMPANY_FALSE:
        return {
          ...state,
          isLoadingCompany: false,
        };

        case SET_LOADING_DOMAIN_HOME_TRUE:
          return {
            ...state,
            isLoadingDomainHome: true,
          };
        case SET_LOADING_DOMAIN_HOME_FALSE:
          return {
            ...state,
            isLoadingDomainHome: false,
          };

          case SET_LOADING_USER_TRUE:
            return {
              ...state,
              isLoadingUser: true,
            };
          case SET_LOADING_USER_FALSE:
            return {
              ...state,
              isLoadingUser: false,
            };

    default:
      return state;
      
  }
};

export default reducer