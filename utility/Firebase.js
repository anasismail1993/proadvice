import * as firebase from 'firebase';

// Optionally import the services that you want to use
//import "firebase/auth";
//import "firebase/database";
//import "firebase/firestore";
//import "firebase/functions";
//import "firebase/storage";

// Initialize Firebase
const firebaseConfig = {
    apiKey: "AIzaSyBtXQhICV4xOzzxI3vWhrhFO7cxjQwZ4_c",
    authDomain: "proadvice-6ac32.firebaseapp.com",
    databaseURL: "https://proadvice-6ac32.firebaseio.com",
    projectId: "proadvice-6ac32",
    storageBucket: "proadvice-6ac32.appspot.com",
    messagingSenderId: "987255700485",
    appId: "1:987255700485:web:44694b7cbb9efda305b06c",
    measurementId: "G-BL2RJ6BDF5"
};

export default firebase.initializeApp(firebaseConfig);