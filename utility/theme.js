export const DarkTheme = {
    dark: true,
    colors: {
      primary: 'red',
      background: '#1c1e21',
      card: '#282a36',
      text: 'white',
      subText: '#a697ce',
      border: 'rgb(199, 199, 204)',
    },
  };
  
  export const LightTheme = {
    dark: false,
    colors: {
      primary: 'red',
      background: 'white',
      card: 'white',
      text: 'black',
      subText: 'grey',
      border: 'rgb(199, 199, 204)',
    },
    fonts: {
      regular: '',
    },
  };